﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailManager : MonoBehaviour
{
    [SerializeField] bool isPlayer;

    Trail trail;
    const int UiLayer = 9;

    private void Awake()
    {
        trail = GetComponentInChildren<Trail>();
    }

    private void Start()
    {
        AddTrail();
    }

    private void OnEnable()
    {
        AddTrail();
    }

    public void AddTrail()
    {
        if (trail)
            Destroy(trail.gameObject);

        if (GameManager.instance)
        {
            GameObject trailInstance = Instantiate(GameManager.instance.GetTrailPrefab(), transform);
            trail = trailInstance.GetComponentInChildren<Trail>();
        }
        
        if (!isPlayer)
            trail.gameObject.layer = UiLayer;
    }

    public GameObject AddTrail(TrailData trailData)
    {
        if (trail)
            Destroy(trail.gameObject);

        trail = Instantiate(trailData.GetTrailPrefab(), transform).GetComponentInChildren<Trail>();
        
        if (!isPlayer)
            trail.gameObject.layer = UiLayer;

        return trail.gameObject;
    }

    public void DetachTrail()
    {
        Trail trail = GetComponentInChildren<Trail>();
        trail.Stop();
        trail.transform.SetParent(null);
        Destroy(trail.gameObject, 5);
    }

    public void UpdateTrail(Vector3 diff)
    {
        trail.UpdatePoints(diff);
    }

    public Trail GetTrail()
    {
        return trail;
    }
}
