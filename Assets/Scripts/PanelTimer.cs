﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class PanelTimer : MonoBehaviour {

    [SerializeField] Image timerBar;
    [SerializeField] float time;
    [SerializeField] float defaultSizeDeltaX;
    [SerializeField] bool getInput;
    [SerializeField] UnityEvent onEnable;
    [SerializeField] UnityEvent canSkip;
    [SerializeField] UnityEvent timeUp;

    float timer;

    private void Update()
    {
       if (timer > 0)
        {
            timer -= Time.deltaTime;

            timerBar.rectTransform.sizeDelta = new Vector2((timer / time) * defaultSizeDeltaX, timerBar.rectTransform.sizeDelta.y);
        }
        else
            TimeUp();

        if (getInput && timer < (time / 4) * 3)
        {
            canSkip.Invoke();

            if (Input.touchCount > 0
                && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                TouchInput touchInput = InputManager.GetTouchInput();

                if (touchInput!= null && touchInput.GetTouchBegan())
                    TimeUp();
            }
        }
    }

    private void OnEnable()
    {
        ResetTime();
        onEnable.Invoke();
    }

    public void ResetTime()
    {
        timer = time;
        timerBar.rectTransform.sizeDelta = new Vector2(defaultSizeDeltaX, timerBar.rectTransform.sizeDelta.y);
    }

    public void TimeUp()
    {
        timeUp.Invoke();
    }

}
