﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class InputManager {

    static TouchInput touchInput = null;
    static bool hasBegan;
    static bool isOverUI;
    static Vector2 position;

    public static TouchInput GetTouchInput()
    {
        touchInput = null;

#if UNITY_EDITOR
        // Check if finger is not over a UI element
        //isOverUI = EventSystem.current.IsPointerOverGameObject();

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
        {
            hasBegan = Input.GetMouseButtonDown(0);
            position = Input.mousePosition;

            touchInput = new TouchInput(hasBegan, position);
        }
        
#elif UNITY_ANDROID

        if (Input.touchCount > 0)
        {
            Touch touch;
            touch = Input.GetTouch(0);
            hasBegan = (touch.phase == TouchPhase.Began);
            position = touch.position;

            touchInput = new TouchInput(hasBegan, position);
        }
#endif

        return touchInput;
    }

}

public class TouchInput
{
    bool touchBegan;
    Vector2 position;

    public TouchInput(bool _touchBegan, Vector2 _position)
    {
        touchBegan = _touchBegan;
        position = _position;
    }

    public bool GetTouchBegan()
    {
        return touchBegan;
    }

    public Vector2 GetPosition()
    {
        return position;
    }
}