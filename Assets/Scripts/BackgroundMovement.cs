﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {

    [SerializeField] Material material;
    [SerializeField] float speed;

    Vector2 offset = Vector2.zero;
    float yOffset = 0;

    private void Awake()
    {
        offset = material.mainTextureOffset;
    }

    private void Update()
    {
        if (!GameManager.instance.InGame)
            return;

        yOffset += (speed * Time.deltaTime);

        if (yOffset >= 1)
            yOffset = 0;

        offset = new Vector2(offset.x, yOffset);

        material.mainTextureOffset = offset;
    }
}
