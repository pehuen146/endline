﻿using System;
using UnityEngine;

public class Environment : MonoBehaviour {

    [SerializeField] float velocity;
    [SerializeField] Transform[] coinSpawners;

    private void Start()
    {
        foreach (var item in coinSpawners)
            Instantiate(GameManager.instance.GetCoinPrefab(), item.position, item.rotation, transform);
    }

    private void Instantiate(Func<GameObject> getCoinPrefab, Vector3 position, Quaternion rotation)
    {
        throw new NotImplementedException();
    }

    void Update ()
    {
        if (GameManager.instance.InGame)
        transform.position += Vector3.down * velocity * Time.deltaTime;
	}
}
