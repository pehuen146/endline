﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedCanvasSingleton : MonoBehaviour {
    
    public static RewardedCanvasSingleton instance;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    public void CloseCanvas()
    {
        gameObject.SetActive(false);
    }
}
