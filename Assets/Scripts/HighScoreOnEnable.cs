﻿using UnityEngine;
using UnityEngine.UI;

public class HighScoreOnEnable : MonoBehaviour {

    Text highScoreText;

    private void Awake()
    {
        highScoreText = GetComponent<Text>();
    }

    private void OnEnable()
    {
        UpdateHighScore();
    }

    private void Start()
    {
        UpdateHighScore();
    }

    void UpdateHighScore()
    {
        if (highScoreText && GameManager.instance)
            highScoreText.text = "Récord: " + GameManager.instance.GetHighScore();
    }
}
