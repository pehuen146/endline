﻿using System;
using UnityEngine;

public abstract class SelectableItemData : ScriptableObject
{
    [SerializeField] Sprite uiImage;
    [SerializeField] ItemPrice ItemPrice;
    [SerializeField] public bool owned = true;

    public Sprite GetUiImage()
    {
        return uiImage;
    }

    public ItemPrice GetItemPrice()
    {
        return ItemPrice;
    }

    public bool GetOwned()
    {
        return owned;
    }

    public abstract bool Equip(uint index);
    
}

[Serializable]
public class ItemPrice : System.Object
{
    [SerializeField] uint price;
    [SerializeField] Currency currency;
    
    public uint GetPrice()
    {
        return price;
    }

    public Currency GetCurrency()
    {
        return currency;
    }
}

public class InGameItem
{
    bool owned;

    public bool GetOwned()
    {
        return owned;
    }
}

[Serializable]
public class InGameShipItem : InGameItem
{
    [SerializeField] ShipData itemData;

    public ShipData GetData()
    {
        return itemData;
    }
}

[Serializable]
public class InGameTrailItem : InGameItem
{
    [SerializeField] TrailData itemData;

    public TrailData GetData()
    {
        return itemData;
    }
}

public enum Currency { Coin, RewardedAd };