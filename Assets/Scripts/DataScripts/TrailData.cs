﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Trail"))]
public class TrailData : SelectableItemData
{
    [SerializeField] GameObject trailPrefab;

    public GameObject GetTrailPrefab()
    {
        return trailPrefab;
    }

    public override bool Equip(uint index)
    {
        return GameManager.instance.SetTrail(index);
    }
}
