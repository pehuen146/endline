﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Model"))]
public class ModelData : ScriptableObject
{
    [SerializeField] GameObject brokenShip;
    [SerializeField] Mesh spaceshipModel;

    public Mesh GetSpaceshipModel()
    {
        return spaceshipModel;
    }

    public GameObject GetBrokenShip()
    {
        return brokenShip;
    }
}
