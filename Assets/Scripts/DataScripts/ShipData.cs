﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Spaceship"))]
public class ShipData : SelectableItemData
{
    [SerializeField] ModelData modelData;
    [SerializeField] Material material;

    public Mesh GetSpaceshipModel()
    {
        return modelData.GetSpaceshipModel();
    }

    public GameObject GetBrokenShip()
    {
        return modelData.GetBrokenShip();
    }

    public Material GetMaterial()
    {
        return material;
    }

    public override bool Equip(uint index)
    {
        return GameManager.instance.SetShip(index);
    }
}
