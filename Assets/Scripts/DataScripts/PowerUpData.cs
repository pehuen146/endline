﻿using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/PowerUp"))]
public abstract class PowerUpData : ScriptableObject
{
    [SerializeField] GameObject powerUpPrefab;
    [SerializeField] Sprite icon;
    [SerializeField] Color iconColor;
    [SerializeField] float time;

    bool active = false;
    float timer = 0;

    public virtual void Activate()
    {
        timer = time;
        active = true;
    }

    public virtual void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        else
            active = false;
    }
}
