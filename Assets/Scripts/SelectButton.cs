﻿using UnityEngine;
using UnityEngine.UI;
public class SelectButton : MonoBehaviour {

    [SerializeField] GameObject selectedObject;
    [SerializeField] Color equipedColor;
    [SerializeField] Color ownedColor;

    Button button;
    Image image;
    ScrollScript scrollScript;

    uint index;
    bool owned = false;
    bool equiped = false;

    void Awake()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();

        button.onClick.AddListener(Select);
    }

    private void Start()
    {
        scrollScript = GetComponentInParent<ScrollScript>();
    }

    public void SetUiImage(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void SetIndex(uint _index)
    {
        index = _index;
    }

    void Select()
    {
        scrollScript.Select(index);
    }

    public void SetOwned(bool value)
    {
        owned = value;
        UpdateButton();
    }

    public bool GetOwned()
    {
        return owned;
    }

    public void SetEquiped(bool value)
    {
        equiped = value;

        if (equiped)
            owned = true;

        UpdateButton();
    }

    public bool GetEquiped()
    {
        return equiped;
    }

    void UpdateButton()
    {
        selectedObject.SetActive(owned);

        if (owned)
        {
            Image image = selectedObject.GetComponent<Image>();

            image.color = ownedColor;

            if (equiped)
                image.color = equipedColor;
        }
    }
}
