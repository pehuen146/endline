﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    [SerializeField] float timelapse;
    [SerializeField] GameObject[] obstacles;

    List<GameObject> obstacleInstances=new List<GameObject>();

    float timer = 0;

	void Update ()
    {
        if (GameManager.instance.InGame)
        {
            timer += Time.deltaTime;

            if (timer >= timelapse)
            {
                timer = 0;
                GameObject instance = Instantiate<GameObject>(obstacles[Random.Range(0, obstacles.Length)], transform.position, Quaternion.identity);
                obstacleInstances.Add(instance);
            }
        }
    }

    public void ResetObstacles()
    {
        while (obstacleInstances.Count > 0)
        {
            GameObject instance = obstacleInstances[0];
            obstacleInstances.RemoveAt(0);
            Destroy(instance);
        }
        //obstacleInstances.Clear();
    }


}
