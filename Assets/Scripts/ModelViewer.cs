﻿using UnityEngine;
using UnityEngine.UI;

public class ModelViewer : MonoBehaviour {

    [SerializeField] RenderTexture defaultRenderTexture;
    [SerializeField] Camera viewCamera;
    [SerializeField] RawImage rawImage;

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    RenderTexture renderTexture;

    private void Awake()
    {
        renderTexture = new RenderTexture(defaultRenderTexture);
        viewCamera.targetTexture = renderTexture;

        if (rawImage)
            rawImage.texture = renderTexture;

        meshFilter = GetComponentInChildren<MeshFilter>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

    public RenderTexture GetRenderTexture()
    {
        return renderTexture;
    }

    public void SetData(ShipData data)
    {
        meshFilter.mesh = data.GetSpaceshipModel();
        meshRenderer.material = data.GetMaterial();
    }
}
