﻿using UnityEngine;
using UnityEngine.UI;

public class ShieldButton : MonoBehaviour
{

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        
        button.onClick.AddListener(ActivateShield);
    }

    void ActivateShield()
    {
        Player.instance.ActivateShield();
    }
}
