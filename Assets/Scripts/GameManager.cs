﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    const string explosionTrigger = "Explosion";

    [SerializeField] Transform spawn;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject crashCanvas;
    [SerializeField] GameObject hudCanvas;
    [SerializeField] GameObject coinPrefab;
    [SerializeField] GameObject magnetUI;
    [SerializeField] GameObject newHighScoreUI;
    [SerializeField] Color32 coinColor;
    [SerializeField] Color videoColor;
    [SerializeField] ObstacleSpawner obstacles;
    [SerializeField] Text coinsText;
    [SerializeField] Text scoreText;
    [SerializeField] TrailData[] trailsData;
    [SerializeField] ShipData[] shipsData;

    
    SoundManager soundManager;
    Animator cameraAnimator;

    float cameraSize;

    bool vibration = true;
    bool sound = true;
    bool newHighScore = false;

    uint trailIndex = 0;
    uint shipIndex = 0;
    uint coins = 0;
    uint score = 0;
    uint highScore = 0;

    bool inGame = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        //Making camera fit screen size
        cameraSize = (7.0f / Screen.width * Screen.height / 2.0f);
        Camera.main.orthographicSize = cameraSize;

        Instantiate(playerPrefab, spawn.position, Quaternion.identity);

        UpdateCoinsUI();
        UpdateScoreUI();
        soundManager = GetComponent<SoundManager>();

        cameraAnimator = Camera.main.gameObject.GetComponent<Animator>();
    }

    public float GetCameraSize()
    {
        return cameraSize;
    }

    public uint GetItemIndex(UiItem item)
    {
        switch (item)
        {
            case UiItem.Ship:
                return shipIndex;

            case UiItem.Trail:
                return trailIndex;
        }
        return 0;
    }

    public void MagnetActive(bool value)
    {
        magnetUI.SetActive(value);
        magnetUI.GetComponent<PanelTimer>().ResetTime();

        if (value == false)
            Player.instance.DeactivateMagnet();
    }

    public GameObject GetTrailPrefab()
    {
        return trailsData[trailIndex].GetTrailPrefab();
    }

    public ShipData GetShipData()
    {
        return shipsData[shipIndex];
    }

    public ShipData GetShipData(uint index)
    {
        return shipsData[index];
    }

    public SelectableItemData GetData(uint index, UiItem item)
    {
        switch (item)
        {
            case UiItem.Ship:
                return shipsData[index];
            case UiItem.Trail:
                return trailsData[index];
        }
        return null;
    }

    public SelectableItemData[] GetData(UiItem item)
    {
        switch (item)
        {
            case UiItem.Ship:
                return shipsData;
            case UiItem.Trail:
                return trailsData;
        }
        return null;
    }

    public TrailData GetTrailData(uint index)
    {
        return trailsData[index];
    }

    public GameObject GetCoinPrefab()
    {
        return coinPrefab;
    }

    public bool SetTrail(uint index)
    {
        TrailData trailData = trailsData[index];

        if (trailData.owned || BuyItem(trailData))
        {
            trailIndex = index;
            return true;
        }

        return false;
    }

    public bool SetShip(uint index)
    {
        ShipData shipData = shipsData[index];

        if (shipData.owned || BuyItem(shipData))
        {
            shipIndex = index;
            return true;
        }

        return false;

    }

    bool BuyItem(SelectableItemData itemData)
    {
        uint price = itemData.GetItemPrice().GetPrice();

        if (coins >= price)
        {
            coins -= price;
            UpdateCoinsUI();
            itemData.owned = true;
            return true;
        }

        return false;
    }

    public bool InGame
    {
        get { return inGame; }
        set { inGame = value; }
    }

    public void RestartGame()
    {
        score = 0;
        UpdateScoreUI();

        newHighScore = false;
        inGame = true;
        Player.instance.gameObject.SetActive(true);
        hudCanvas.SetActive(true);
        Time.timeScale = 1;
        obstacles.ResetObstacles();
        Player.instance.Respawn(spawn.position);

        MagnetActive(false);
    }

    public void ContinueGame()
    {
        inGame = true;
        Player.instance.gameObject.SetActive(true);
        hudCanvas.SetActive(true);
        Time.timeScale = 1;
        obstacles.ResetObstacles();
        Player.instance.Respawn(spawn.position);

        MagnetActive(false);
    }

    public void EndGame()
    {
        newHighScoreUI.SetActive(newHighScore);

        inGame = false;
        Player.instance.gameObject.SetActive(false);
        hudCanvas.SetActive(false);
        Time.timeScale = 1;
        obstacles.ResetObstacles();
    }

    public void Crash()
    {
        if (vibration)
            Handheld.Vibrate();

        inGame = false;
        hudCanvas.SetActive(false);
        magnetUI.SetActive(false);

        soundManager.PlayExplosionSound();

        cameraAnimator.SetTrigger(explosionTrigger);

        StartCoroutine(CrashPanelOpen());
    }

    IEnumerator CrashPanelOpen()
    {
        yield return new WaitForSeconds(2);
        crashCanvas.SetActive(true);
    }

    public ShipData[] GetShipsData()
    {
        return shipsData;
    }

    public TrailData[] GetTrailsData()
    {
        return trailsData;
    }

    public void SetSoundValue(bool value)
    {
        sound = value;
    }

    public void SetVibrationValue(bool value)
    {
        vibration = value;
    }

    public bool GetVibrationValue()
    {
        return vibration;
    }

    public bool GetSoundValue()
    {
        return sound;
    }

    public void AddCoin()
    {
        coins++;
        UpdateCoinsUI();
        soundManager.PlayCoinSound();
    }

    void UpdateCoinsUI()
    {
        coinsText.text = coins.ToString();
    }

    void UpdateScoreUI()
    {
        scoreText.text = score.ToString();
    }

    public uint GetCoins()
    {
        return coins;
    }

    public string GetCoinFormat()
    {
        string format = "<color=#" + ColorUtility.ToHtmlStringRGBA(coinColor) + ">{</color>";

        return format;
    }

    public string GetVideoFormat()
    {
        string format = "<color=#" + ColorUtility.ToHtmlStringRGBA(videoColor) + ">}</color>";

        return format;
    }

    public void AddPoint()
    {
        score++;
        UpdateScoreUI();

        if (score > highScore)
        {
            highScore = score;

            if (!newHighScore)
                newHighScore = true;
        }
    }

    public uint GetHighScore()
    {
        return highScore;
    }

    public uint GetScore()
    {
        return score;
    }
}
