﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreOnEnable : MonoBehaviour {

    Text ScoreText;

    private void Awake()
    {
        ScoreText = GetComponent<Text>();
    }

    private void OnEnable()
    {
        UpdateHighScore();
    }

    private void Start()
    {
        UpdateHighScore();
    }

    void UpdateHighScore()
    {
        if (ScoreText && GameManager.instance)
            ScoreText.text = GameManager.instance.GetScore().ToString();
    }
}
