﻿using UnityEngine;
using UnityEngine.UI;

public class RestartButton : MonoBehaviour {

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(Restart);
    }

    void Restart()
    {
        GameManager.instance.RestartGame();
    }
}
