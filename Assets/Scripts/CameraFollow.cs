﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    
    [SerializeField] Transform Player;

	void Update ()
    {
        if (Player != null)
            transform.position = new Vector3(transform.position.x, Player.position.y, transform.position.z);
	}


}
