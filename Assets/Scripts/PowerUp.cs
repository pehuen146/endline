﻿using UnityEngine;

public abstract class PowerUp {

    [SerializeField] PowerUpData data;

    public abstract void Activate();
    public abstract void Deactivate();
}
