﻿using UnityEngine;
using UnityEngine.UI;

public class ShowAdOnClick : MonoBehaviour {

    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(ShowAd);
    }

    void ShowAd()
    {
        AdController.instance.ShowRewardedVideo();
    }
}
