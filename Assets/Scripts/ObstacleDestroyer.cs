﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDestroyer : MonoBehaviour {

    [SerializeField] string obstacleTag;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(obstacleTag))
            Destroy(collision.gameObject);
    }
}
