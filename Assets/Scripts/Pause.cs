﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

	public void PauseGame () {
        Time.timeScale = 0;
        GameManager.instance.InGame = false;
	}
	
	public void ContinueGame()
    {
        Time.timeScale = 1;
        GameManager.instance.InGame = true;
    }
}
