﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollScript : MonoBehaviour {

    [SerializeField] ScrollRect scrollView;
    [SerializeField] GameObject scrollContent;
    [SerializeField] GameObject shipViewPrefab;
    [SerializeField] GameObject selectButtonPrefab;
    [SerializeField] RawImage shipView;
    [SerializeField] UiItem item;
    [SerializeField] Text equipButtonText;
    [SerializeField] Button equipButton;
    [SerializeField] CoinsText coinsText;

    GameObject shipViewObj;
    ModelViewer modelViewer;
    SelectableItemData[] itemsData;
    List<SelectButton> selectButtons = new List<SelectButton>();

    uint selected = 0;

    private void Awake()
    {
        shipViewObj = Instantiate(shipViewPrefab, Vector3.zero , Quaternion.identity);
        modelViewer = shipViewObj.GetComponent<ModelViewer>();

        equipButton.onClick.AddListener(Equip);

        itemsData = GameManager.instance.GetData(item);
    }

    private void Start()
    {
        for (uint i = 0; i < itemsData.Length; i++)
            GenerateItem(i, itemsData[i].GetOwned());

        shipView.texture = modelViewer.GetRenderTexture();

        selected = GameManager.instance.GetItemIndex(item);
        
        SetEquiped(selected);
    }

    private void OnEnable()
    {
        modelViewer.SetData(GameManager.instance.GetShipData());

        modelViewer.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        if (modelViewer)
            modelViewer.gameObject.SetActive(false);
    }

    void GenerateItem(uint index, bool owned)
    {
        //Character select buttons creation
        GameObject selectButtonObj = Instantiate(selectButtonPrefab);
        SelectButton selectButton = selectButtonObj.GetComponent<SelectButton>();
        
        selectButton.SetUiImage(itemsData[index].GetUiImage());
        
        selectButton.SetIndex(index);

        selectButton.SetOwned(itemsData[index].GetOwned());

        selectButtons.Add(selectButton);

        selectButtonObj.transform.SetParent(scrollContent.transform,false);
    }

    public void Select(uint index)
    {
        if (index == selected)
        {
            Equip();
            return;
        }

        selected = index;

        UpdateEquipButtonText();


        switch (item)
        {
            case UiItem.Ship:
                ShipData selectedShipData = GameManager.instance.GetShipData(index);
                modelViewer.SetData(selectedShipData);
                break;

            case UiItem.Trail:
                TrailData trailData = GameManager.instance.GetTrailData(index);
                modelViewer.GetComponentInChildren<TrailManager>().AddTrail(trailData);
                break;
        }
    }

    void Equip()
    {
        bool canEquip = false;

        switch (item)
        {
            case UiItem.Ship:
                canEquip = GameManager.instance.SetShip(selected);
                break;

            case UiItem.Trail:
                canEquip = GameManager.instance.SetTrail(selected);
                break;
        }
        if (canEquip)
        {
            coinsText.UpdateCoins();
            UpdateEquipButtonText();
            SetEquiped(selected);
        }
        
    }

    void UpdateEquipButtonText()
    {
        SelectButton selectButton = selectButtons[(int)selected];

        uint price = itemsData[selected].GetItemPrice().GetPrice();

        if (selectButton.GetOwned())
            equipButtonText.text = "Equipar";
        else
            equipButtonText.text = GameManager.instance.GetCoinFormat() + price.ToString();

        equipButton.interactable = !selectButton.GetEquiped();

        if (!itemsData[selected].GetOwned() && price > GameManager.instance.GetCoins())
            equipButton.interactable = false;
    }

    public void SetRawImageTexture(Texture texture)
    {
        shipView.texture = texture;
    }

    void SetEquiped(uint index)
    {
        for (int i = 0; i < selectButtons.Count; i++)
        {
            bool selected = (i == index);
            selectButtons[i].SetEquiped(selected);
        }

        UpdateEquipButtonText();
    }
}

public enum UiItem { Ship, Trail };
