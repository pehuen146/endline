﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance;

    [SerializeField] Transform ghostPlayerPrefab;
    [SerializeField] float speed;
    [SerializeField] AudioClip explosionSFX;
    [SerializeField] GameObject sideCameraPrefab;
    [SerializeField] GameObject shield;

    const string ObstacleTag = "Obstacle";
    const string PointTag = "Point";
    const string MagnetTag = "Magnet";
    const string ShieldTag = "Shield";
    const int ghostsAmount = 2;

    bool magnetActive = false;
    bool shieldActive = false;
    Transform leftGhost;
    Transform rightGhost;

    Transform[] ghosts = new Transform[2];

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    float currentSpeed;
    float screenWidth;
    TrailManager trailManager;
    Animator animator;

    MovementDirection movDirection = MovementDirection.Forward;
    MovementState movState = MovementState.SideToSide;

    ShipData shipData;

    private void OnEnable()
    {
        shipData = GameManager.instance.GetShipData();
        meshFilter.mesh = shipData.GetSpaceshipModel();
        meshRenderer.material = shipData.GetMaterial();

        movDirection = MovementDirection.Forward;
    }

    public bool GetMagnetActive()
    {
        return magnetActive;
    }

    public float GetScreenWidth()
    {
        return screenWidth;
    }

    public void DeactivateMagnet()
    {
        magnetActive = false;
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        trailManager = GetComponent<TrailManager>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        meshFilter = GetComponentInChildren<MeshFilter>();
        animator = GetComponentInChildren<Animator>();

        gameObject.SetActive(false);
    }

    private void Start()
    {
        currentSpeed = speed;

        shipData = GameManager.instance.GetShipData();

        DestroyImmediate(GetComponent<GhostPlayer>());

        //Finding screen Width
        Camera cam = Camera.main;
        Vector3 screenBottomLeft = cam.ViewportToWorldPoint(new Vector3(0, 0, transform.position.z));
        Vector3 screenTopRight = cam.ViewportToWorldPoint(new Vector3(1, 1, transform.position.z));

        screenWidth = screenTopRight.x - screenBottomLeft.x;
        
        CreateSideCameras(GameManager.instance.GetCameraSize());

        CreateGhosts();
        PositionGhosts();

        ghosts[0] = rightGhost.transform;
        ghosts[1] = leftGhost.transform;
    }

    void Update()
    {
        if (!GameManager.instance.InGame)
            return;

        PositionGhosts();

        if (movState == MovementState.Normal)
            movDirection = MovementDirection.Forward;

        TouchInput touchInput = GetTouchInput(); 

        // Check if there is a touch
        if (touchInput != null)
        {
#if !UNITY_EDITOR
            // Check if finger is not over a UI element
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
#endif
            switch (movState)
            {
                case MovementState.SideToSide:
                    SideToSideMovement(touchInput);
                    break;

                case MovementState.Normal:
                    NormalMovement(touchInput);
                    break;
            }
#if !UNITY_EDITOR
            }
#endif
        }

        Vector2 mov = Vector2.zero;

        switch (movDirection)
        {
            case MovementDirection.Left:
                mov = Vector2.left;
                break;

            case MovementDirection.Right:
                mov = Vector2.right;
                break;

            case MovementDirection.Forward:
                 mov = Vector2.zero;
                break;
        }

        float animationDirection = mov.x;
        animationDirection = Mathf.Lerp(animator.GetFloat("Direction"), animationDirection, 0.25f);

        animator.SetFloat("Direction", animationDirection);

        mov.Normalize();

        mov *= currentSpeed * Time.deltaTime;

        Vector3 direction = new Vector3(mov.x, mov.y, 0.0f);
        transform.position += direction;

        CheckOutOfScreen();
    }

    void CheckOutOfScreen()
    {
        if (gameObject.activeInHierarchy && !IsVisible())
        {
            SwapPlayer();
        }
    }

    bool IsVisible()
    {
        if (transform.position.x < screenWidth /2 && transform.position.x > -screenWidth /2)
            return true;

        return false;
    }

    public void ActivateShield()
    {
        shield.SetActive(true);
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObstacleTag))
        {

            GameObject brokenShipPrefab = shipData.GetBrokenShip();
            GameObject brokenShipInstance = Instantiate(brokenShipPrefab, transform.position, transform.rotation);

            MeshRenderer[] brokenMeshes = brokenShipInstance.GetComponentsInChildren<MeshRenderer>();

            foreach (var brokenMesh in brokenMeshes)
                brokenMesh.material = shipData.GetMaterial();

            Destroy(brokenShipInstance, 4);

            float radius = 5.0F;
            float power = 10.0F;

            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                {
                    Vector3 direction = Random.insideUnitCircle.normalized * power;
                    rb.AddForce(direction, ForceMode.Impulse);
                }
            }

            GameManager.instance.Crash();
            trailManager.DetachTrail();
            gameObject.SetActive(false);

            return;
        }
        else if (collision.CompareTag(PointTag))
            GameManager.instance.AddPoint();
        else if (collision.CompareTag(MagnetTag))
        {
            magnetActive = true;
            GameManager.instance.MagnetActive(true);
            Destroy(collision.gameObject);
        }
    }

    public void Respawn(Vector3 position)
    {
        transform.position = position;
        currentSpeed = speed;
    }

    void CreateSideCameras(float cameraSize)
    {
        GameObject leftSideCamera = Instantiate(sideCameraPrefab, Vector3.zero, transform.rotation);
        GameObject rightSideCamera = Instantiate(sideCameraPrefab, Vector3.zero, transform.rotation);

        leftSideCamera.GetComponent<Camera>().orthographicSize = cameraSize;
        rightSideCamera.GetComponent<Camera>().orthographicSize = cameraSize;

        var cameraPosition = transform.position;
        cameraPosition.z = -10;

        // Right
        cameraPosition.x = transform.position.x + screenWidth;
        cameraPosition.y = transform.position.y;
        rightSideCamera.transform.position = cameraPosition;

        // Left
        cameraPosition.x = transform.position.x - screenWidth;
        cameraPosition.y = transform.position.y;
        leftSideCamera.transform.position = cameraPosition;
    }

    void CreateGhosts()
    {
        leftGhost = Instantiate(ghostPlayerPrefab, Vector3.zero, transform.rotation) as Transform;
        DestroyImmediate(leftGhost.GetComponent<Player>());

        rightGhost = Instantiate(ghostPlayerPrefab, Vector3.zero, transform.rotation) as Transform;
        DestroyImmediate(rightGhost.GetComponent<Player>());
    }

    void PositionGhosts()
    {
        var ghostPosition = transform.position;

        // Right
        ghostPosition.x = transform.position.x + screenWidth;
        ghostPosition.y = transform.position.y;
        rightGhost.position = ghostPosition;

        // Left
        ghostPosition.x = transform.position.x - screenWidth;
        ghostPosition.y = transform.position.y;
        leftGhost.position = ghostPosition;
    }

    void SwapPlayer()
    {
        PositionGhosts();

        foreach (var ghost in ghosts)
        {
            if (ghost.transform.position.x < screenWidth && ghost.transform.position.x > -screenWidth)
            {
                Vector3 position = transform.position;
                transform.position = new Vector2(ghost.transform.position.x, transform.position.y);

                Vector3 diff = transform.position - position;

                trailManager.UpdateTrail(diff);

                break;
            }
        }
    }

    void SideToSideMovement(TouchInput touchInput)
    {
        if (!touchInput.GetTouchBegan())
            return;

        switch (movDirection)
        {
            case MovementDirection.Left:
                movDirection = MovementDirection.Right;
                break;
            case MovementDirection.Right:
                movDirection = MovementDirection.Left;
                break;
            default:
                Vector2 position = touchInput.GetPosition();

                if (position.x < Screen.width / 2)
                    movDirection = MovementDirection.Left;
                else if (position.x > Screen.width / 2)
                    movDirection = MovementDirection.Right;
                break;
        }
    }

    void NormalMovement(TouchInput touchInput)
    {
        Vector2 position = touchInput.GetPosition();

        if (position.x < Screen.width / 2)
            movDirection = MovementDirection.Left;
        else if (position.x > Screen.width / 2)
            movDirection = MovementDirection.Right;
    }

    public TouchInput GetTouchInput()
    {
        TouchInput touchInput = null;
        bool hasBegan;
        Vector2 position;

        if (Input.touchCount > 0)
        {
            Touch touch;
            touch = Input.GetTouch(0);
            hasBegan = (touch.phase == TouchPhase.Began);
            position = touch.position;

            touchInput = new TouchInput(hasBegan, position);
        }
        else if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
        {
            hasBegan = Input.GetMouseButtonDown(0);
            position = Input.mousePosition;

            touchInput = new TouchInput(hasBegan, position);
        }

        return touchInput;
    }
}

    public enum MovementDirection { Left, Right, Forward};
    public enum MovementState { SideToSide, Normal };