﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    [SerializeField] float speed = 5;
    [SerializeField] float minimumDistance = 1;

    new Rigidbody2D rigidbody;
    bool magnetized = false;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.instance.AddCoin();
            Destroy(gameObject);
        }
    }

    private void Update()
    {

        Player player = Player.instance;
        Transform playerTransform = player.transform;

        if (player && player.GetMagnetActive() && GameManager.instance.InGame)
        {
            float screenWidth = player.GetScreenWidth();

            Vector2 diff = playerTransform.position - transform.position;
            Vector2 rightDiff = new Vector2(playerTransform.position.x + screenWidth, playerTransform.position.y) - (Vector2)transform.position;
            Vector2 leftDiff = new Vector2(playerTransform.position.x - screenWidth, playerTransform.position.y) - (Vector2)transform.position;

            if (diff.magnitude > rightDiff.magnitude)
                diff = rightDiff;
            else if (diff.magnitude > leftDiff.magnitude)
                diff = leftDiff;

            if (!magnetized && diff.magnitude < minimumDistance)
                magnetized = true;

            float velocity = rigidbody.velocity.magnitude;
            
            if (magnetized)
                rigidbody.velocity = Mathf.Lerp(velocity, speed, .1f) * diff.normalized;

            float rightEdge = 0 + screenWidth / 2;
            float leftEdge = 0 - screenWidth / 2;

            if (transform.position.x > rightEdge)
                transform.position = new Vector2(leftEdge, transform.position.y);
            else if (transform.position.x < leftEdge)
                transform.position = new Vector2(rightEdge, transform.position.y);
        }
        else
        {
            rigidbody.velocity = Vector2.zero;
        }

    }

}
