﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostPlayer : MonoBehaviour {
    Animator animator;

	void Start () {

        animator = GetComponentInChildren<Animator>();
	}

    public void SetDirection(float direction)
    {
        animator.SetFloat("Direction", direction);
    }

}
