﻿using UnityEngine;
using UnityEngine.UI;

public class CoinsText : MonoBehaviour {

    Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        UpdateCoins();
    }

    public void UpdateCoins()
    {
        if (text)
            text.text = GameManager.instance.GetCoinFormat() + GameManager.instance.GetCoins().ToString();
    }
}
