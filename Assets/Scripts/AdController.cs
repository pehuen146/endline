﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdController : MonoBehaviour {
    
    public static AdController instance;

    private RewardBasedVideoAd rewardedVideo;

    const string appId = "ca-app-pub-7605693134218621~4444836536";

#if UNITY_ANDROID
    const string rewardedVideoId = "ca-app-pub-3940256099942544/5224354917";
#else
    const string rewardedVideoId = "unexpected_platform";
#endif

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    private void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        // Get singleton reward based video ad reference.
        this.rewardedVideo = RewardBasedVideoAd.Instance;
        
        // Called when the ad is closed.
        rewardedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the user should be rewarded for watching a video.
        rewardedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

        RequestRewardedVideo();
    }

    private void RequestRewardedVideo()
    {
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardedVideo.LoadAd(request, rewardedVideoId);
    }

    public void ShowRewardedVideo()
    {
        if (rewardedVideo.IsLoaded())
            rewardedVideo.Show();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        
        this.RequestRewardedVideo();

        RewardedCanvasSingleton.instance.CloseCanvas();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        GameManager.instance.ContinueGame();
    }
}