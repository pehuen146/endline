﻿using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    Button button;

    void Awake()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(Play);
    }

    void Play()
    {
        GameManager.instance.RestartGame();
    }


}
