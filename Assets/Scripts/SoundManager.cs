﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [SerializeField] AudioClip coinSound;
    [SerializeField] AudioClip explosionSound;

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayCoinSound()
    {
        PlaySound(coinSound);
    }

    public void PlayExplosionSound()
    {
        PlaySound(explosionSound);
    }

    void PlaySound(AudioClip clip)
    {
        if (GameManager.instance.GetSoundValue())
            audioSource.PlayOneShot(clip);
    }
}
